# Idn Rails Starter App

This is an example application to demonstarte how to use 'idn_sdk_ruby' library using Ruby On Rail framework. 

Gemfile contains all the required gems list. No need to worry about gems list 'bundle install' will install all the gems. Live application is available on heroku https://idn-app-rails.herokuapp.com  

## Ruby & Rails versions

 - Ruby 2.2.3
	
 - Rails 4.2.4

You can install required versions of ruby, rails & gemsets using [rvm(ruby version manager)](https://rvm.io/)

## Configuration Instructions
 
After installing Ruby & Rails from console use following commands:

	$>git clone https://gitlab.com/wavelabs/idn-app-rails.git
	$>cd idn-app-rails
	$>bundle install
	$>rails server

Open the link http://localhost:3000 in any browser.Now you are ready to use the web application.  

## Contributing

Bug reports and pull requests are welcome on GitHub at https://gitlab.com/wavelabs/idn-app-rails. This project is intended to be a safe, welcoming space for collaboration, and contributors are expected to adhere to the [Contributor Covenant](contributor-covenant.org) code of conduct.

