# This controller is responsible for handling 
# user's login with social accounts 
# requests using omniauth client libraries
# with Wavelabes API Server

class Com::Nbos::Social::SocialController < ApplicationController

 def create
	 api_response = @identity_api.connect(request.env['omniauth.auth'], params[:provider])
	 if api_response[:status] == 200 && !api_response[:data].message.present?
	    @member = api_response[:data]
	    create_session(@member)
	    redirect_to :com_nbos_core_dash_board
   else
	    flash[:notice] = api_response[:data].message
	    redirect_to :com_nbos_core_login
   end
 end

end
